package acronym

import (
	"regexp"
	"strings"
)

// Abbreviate converts a phrase into its acronym
func Abbreviate(s string) string {
	re := regexp.MustCompile(`(?m)[a-zA-Z']*`)

	stringArray := re.FindAllString(s, -1)
	acronymString := ""
	for _, word := range stringArray {
		if len(word) > 0 {
			acronymString += strings.ToUpper(string(word[0]))
		}
	}
	return acronymString
}
