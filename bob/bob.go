package bob

import (
	"strings"
)

func isYelling(remark string) bool {
	return remark == strings.ToUpper(remark) && remark != strings.ToLower(remark)
}

func isAQuestion(remark string) bool {
	return remark[len(remark)-1:] == "?"
}

func Hey(remark string) string {
	remark = strings.TrimSpace(remark)

	if len(remark) < 1 {
		return "Fine. Be that way!"
	}

	if isYelling(remark) && isAQuestion(remark) {
		return "Calm down, I know what I'm doing!"
	} else if isYelling(remark) {
		return "Whoa, chill out!"
	} else if isAQuestion(remark) {
		return "Sure."
	}
	return "Whatever."
}
