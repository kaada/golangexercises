package twelve

var numbers = []string{"first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth"}

var items = []string{"a Partridge in a Pear Tree", "two Turtle Doves",
	"three French Hens", "four Calling Birds", "five Gold Rings", "six Geese-a-Laying",
	"seven Swans-a-Swimming", "eight Maids-a-Milking", "nine Ladies Dancing",
	"ten Lords-a-Leaping", "eleven Pipers Piping", "twelve Drummers Drumming"}

//Song prints The Twelwe Days of Christmas
func Song() string {
	fullSong := ""
	for i := 1; i <= 12; i++ {
		fullSong += Verse(i) + "\n"
	}
	return fullSong[:len(fullSong)-1]
}

//Verse returns a verse in the song
func Verse(number int) string {
	return "On the " + numbers[number-1] + " day of Christmas my true love gave to me: " + getAllItems(number, true) + "."
}

func getAllItems(number int, first bool) string {
	for i := number; i > 0; i++ {
		if i == 1 && !first {
			return "and " + items[i-1]
		} else if i == 1 && first {
			return items[i-1]
		}
		return items[i-1] + ", " + getAllItems(number-1, false)
	}
	return ""
}
