package space

type Planet string

//Age returns number of years in the context of which planet you are on
func Age(seconds float64, planet Planet) float64 {
	secondsInEarthYear := float64(31557600)

	if planet == "Mercury" {
		return seconds / (secondsInEarthYear * 0.2408467)
	} else if planet == "Venus" {
		return seconds / (secondsInEarthYear * 0.61519726)
	} else if planet == "Earth" {
		return seconds / secondsInEarthYear
	} else if planet == "Mars" {
		return seconds / (secondsInEarthYear * 1.8808158)
	} else if planet == "Jupiter" {
		return seconds / (secondsInEarthYear * 11.862615)
	} else if planet == "Saturn" {
		return seconds / (secondsInEarthYear * 29.447498)
	} else if planet == "Uranus" {
		return seconds / (secondsInEarthYear * 84.016846)
	} else if planet == "Neptune" {
		return seconds / (secondsInEarthYear * 164.79132)
	} else {
		return -1
	}
}
