package matrix

import (
	"errors"
	"strconv"
	"strings"
)

type Matrix [][]int

//New ...
func New(matrixIn string) (*Matrix, error) {
	var err error
	lines := strings.Split(matrixIn, "\n")
	m := make(Matrix, len(lines))

	for i, l := range lines {
		ws := strings.Fields(l)

		if i > 0 && len(m[0]) != len(ws) {
			return nil, errors.New("Rows have unequal length")
		}

		row := make([]int, len(ws))
		for j, w := range ws {
			if row[j], err = strconv.Atoi(w); err != nil {
				return nil, errors.New("Invalid int in element data")
			}
		}
		m[i] = row
	}
	return &m, nil
}

func (m *Matrix) Set(row, col, val int) (ok bool) {
	mm := *m

	if row < 0 || row >= len(mm) || col < 0 {
		return false
	}

	if cols := len(mm[0]); col >= cols {
		return false
	}

	mm[row][col] = val
	return true
}

func (m *Matrix) Rows() [][]int {
	mr := make([][]int, len(*m))
	for i, r := range *m {
		mr[i] = append([]int{}, r...)
		//hva med bare r her, ikke noe append?
	}

	return mr
}

func (m *Matrix) Cols() [][]int {
	mm := *m
	if len(mm) == 0 {
		return nil
	}
	mc := make([][]int, len(mm[0]))
	for i := range mc {
		col := make([]int, len(mm))
		for j := range col {
			col[j] = mm[j][i]
		}
		mc[i] = col
	}
	return mc
}
